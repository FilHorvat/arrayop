from unittest import TestCase
from myarray import BaseArray


class TestArray(TestCase):

    def test_to_string(self):
        # 2D:
        arr_2d = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 8, 7, 6, 4, 5))
        compare_str = "1 2 3 8 \n7 6 4 5 \n"
        self.assertEqual(arr_2d._to_string(), compare_str)

        arr_2d = BaseArray((3, 2), dtype=int, data=(-1, -2, -4, -3, -5, -6))
        compare_str = "-1 -2 \n-4 -3 \n-5 -6 \n"
        self.assertEqual(arr_2d._to_string(), compare_str)

        arr_2d = BaseArray((2, 3), dtype=float, data=(1., 6., 3., 4., 3., 2.))
        compare_str = "1.0 6.0 3.0 \n4.0 3.0 2.0 \n"
        self.assertEqual(arr_2d._to_string(), compare_str)

        arr_2d = BaseArray((5, 5), dtype=float, data=(
            1., 2., 10., 4., 5., 6., 7., 5., 9., 10., 1., 2.,
            10., 4., 5., 6., 7., 5., 9., 10., 6., 7., 5., 9., 10.))
        compare_str = "1.0 2.0 10.0 4.0 5.0 \n6.0 7.0 5.0 9.0 10.0 \n" \
                      "1.0 2.0 10.0 4.0 5.0 \n6.0 7.0 5.0 9.0 10.0 \n6.0 7.0 5.0 9.0 10.0 \n"
        self.assertEqual(arr_2d._to_string(), compare_str)

        # 3D:
        arr_3d = BaseArray((2, 4, 2), dtype=int, data=(1, 2, 3, 8, 7, 6, 4,
                                                       5, 1, 2, 3, 8, 7, 6, 4, 5))
        compare_str = "1 2 3 8 \n7 6 4 5 \n\n1 2 3 8 \n7 6 4 5 \n"
        self.assertEqual(arr_3d._to_string(), compare_str)

        arr_3d = BaseArray((3, 2, 3), dtype=int, data=(-1, -2, -4, -3, -5, -6, -1, -2, -4,
                                                       -3, -5, -6, -1, -2, -4, -3, -5, -6))
        compare_str = "-1 -2 \n-4 -3 \n-5 -6 \n\n-1 -2 \n-4 -3 \n-5 -6 \n\n-1 -2 \n-4 -3 \n-5 -6 \n"
        self.assertEqual(arr_3d._to_string(), compare_str)

        arr_3d = BaseArray((2, 3, 5), dtype=float, data=(
            1., 6., 3., 4., 3., 2., 1., 6., 3., 4., 3., 2., 1.,
            6., 3., 4., 3., 2., 1., 6., 3., 4., 3., 2., 1., 6., 3., 4., 3., 2.))
        compare_str = "1.0 6.0 3.0 \n4.0 3.0 2.0 \n\n1.0 6.0 3.0 \n4.0 3.0 2.0 \n\n" \
                      "1.0 6.0 3.0 \n4.0 3.0 2.0 \n\n1.0 6.0 3.0 \n4.0 3.0 2.0 \n\n1.0 6.0 3.0 \n" \
                      "4.0 3.0 2.0 \n"
        self.assertEqual(arr_3d._to_string(), compare_str)

        arr_3d = BaseArray((5, 5, 1), dtype=float, data=(
            1., 2., 10., 4., 5., 6., 7., 5., 9., 10., 1., 2., 10.,
            4., 5., 6., 7., 5., 9., 10., 5., 5., 2., 2., 3.))
        compare_str = "1.0 2.0 10.0 4.0 5.0 \n6.0 7.0 5.0 9.0 10.0 \n" \
                      "1.0 2.0 10.0 4.0 5.0 \n6.0 7.0 5.0 9.0 10.0 \n5.0 5.0 2.0 2.0 3.0 \n"
        self.assertEqual(arr_3d._to_string(), compare_str)

    def test_search_element(self):
        search_ele = 2
        succ_var = False

        # 1D
        test_array = BaseArray((8,), dtype=int, data=(1, 2, 2, 1, 5, 6, 7, 1))
        if len(test_array._search_element(search_ele)) > 1 or\
                len(test_array._search_element(search_ele)) == 0:
            succ_var = True
        self.assertTrue(succ_var)

        # 2D
        succ_var = False
        test_array = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        if len(test_array._search_element(search_ele)) > 1 or\
                len(test_array._search_element(search_ele)) == 0:
            succ_var = True
        self.assertTrue(succ_var)

        # 3D
        succ_var = False
        test_array = BaseArray((3, 3, 2), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2,
                                                           2, 0, 0, 0, 2, 0, 0, 0, 2, 2))
        if len(test_array._search_element(search_ele)) > 1 or\
                len(test_array._search_element(search_ele)) == 0:
            succ_var = True
        self.assertTrue(succ_var)

    def test_sort(self):
        test_array = BaseArray((3, 4), dtype=int, data=(4, 6, 2, -2, 0, 2, -3, 7, 1, -5, 5, 9))
        test_array._sort_array(1)
        for vrs in range(0, test_array.shape[0]):
            for i in range(1, test_array.shape[1]):
                self.assertGreaterEqual(test_array[vrs, i], test_array[vrs, i - 1])

        test_array = BaseArray((3, 4), dtype=int, data=(4, 6, 2, -2, 0, 2, -3, 7, 1, -5, 5, 9))
        test_array._sort_array(2)
        for stol in range(0, test_array.shape[1]):
            for i in range(1, test_array.shape[0]):
                self.assertGreaterEqual(test_array[i, stol], test_array[i - 1, stol])

        test_array = BaseArray((5,), dtype=int, data=(2, -3, 10, 5, 8))
        test_array._sort_array()
        for i in range(1, len(list(test_array))):
            self.assertGreaterEqual(test_array[i], test_array[i - 1])

    def test_scalar_data(self):
        scalar = 2

        test_array = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
        test_data = [2, 4, 6, 2, 10, 12, 14, 2]
        test_array._scalar_mul(scalar)
        self.assertTrue(test_array._compare_data(test_data))

        test_array = BaseArray((4, 2, 2), dtype=int, data=(1, 2, 3, 4, 1, 6, 7,
                                                           8, 1, 10, 11, 1, 13, 1, 15, 1))
        test_data = [0.5, 1.0, 1.5, 2.0, 0.5, 3.0, 3.5, 4.0, 0.5, 5.0, 5.5, 0.5,
                     6.5, 0.5, 7.5, 0.5]
        test_array._scalar_dev(scalar)
        self.assertTrue(test_array._compare_data(test_data))

        test_array = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
        test_data = [3, 4, 5, 3, 7, 8, 9, 3]
        test_array._scalar_sum(scalar)
        self.assertTrue(test_array._compare_data(test_data))

        test_array = BaseArray((4, 2, 2), dtype=int, data=(1, 2, 3, 4, 1, 6, 7,
                                                           8, 1, 10, 11, 1, 13, 1, 15, 1))
        test_data = [-1, 0, 1, 2, -1, 4, 5, 6, -1, 8, 9, -1, 11, -1, 13, -1]
        test_array._scalar_sub(scalar)
        self.assertTrue(test_array._compare_data(test_data))

    def test_addition(self):
        succ = False
        first = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
        second = BaseArray((2, 4), dtype=int, data=(-1, -2, -3, -1, -5, -6, -7, -1))
        if (first._array_addition(second))._is_equal(BaseArray((2, 4), dtype=int, )):
            succ = True
        self.assertTrue(succ)

    def test_subtraction(self):
        succ = False
        test_array = BaseArray((2, 4), dtype=int, data=(-1, -2, -3, -1, -5, -6, -7, -1))
        if (test_array._array_subtraction(test_array))._is_equal(BaseArray((2, 4), dtype=int, )):
            succ = True
        self.assertTrue(succ)

    def test_type(self):
        test_array = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
        test_array._log(10)
        for ele in test_array:
            self.assertTrue(isinstance(ele, float))

        test_array = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
        test_array._scalar_sum(1)
        for ele in test_array:
            self.assertTrue(isinstance(ele, int))

    def test_array_multiplication(self):
        first = BaseArray((2, 3), dtype=int, data=(1, 2, 3, 4, 5, 6))
        second = BaseArray((3, 2), dtype=int, data=(7, 8, 9, 10, 11, 12))
        compare_str = "58 64 \n139 154 \n"
        self.assertEqual(first._mul(second)._to_string(), compare_str)

    def test_raise_exception(self):
        first = BaseArray((4, 4), dtype=int, )
        second = BaseArray((2, 2), dtype=int, )
        with self.assertRaises(Exception):
            first._mul(second)
