from typing import Tuple
from typing import List
from typing import Union
from math import log, exp

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def _is_equal(self,array):
        if self.shape != array.shape:
            return False
        i = 0
        for ele in array:
            if ele != self.__data[i]:
                return False
            i+=1
        return True

    def _to_string(self):
        out = ""
        if len(self.__shape) == 1:
            out = _1D_to_string(self)
        elif len(self.__shape) == 2:
            out = _2D_to_string(self)
        elif len(self.__shape) == 3:
            out = _3D_to_string(self)
        else:
            raise Exception("out of range for to_string")
        return out

    def _sort_array(self,option=1):
        if len(self.shape) > 2:
            raise Exception("Ivalid shape for sorting")
        elif len(self.shape) == 1:
            _merge_sort(self.__data)
        else:
            if option == 1:#vrstice
                self.__data = _sort_by_row(self.__data,self.shape)
            elif option == 2:#stolpci
                self.__data = _sort_by_collumn(self.__data,self.shape)
            else:
                raise Exception("Wrong option for sort")


    def _search_element(self,ele):
        out = []
        if len(self.__shape) == 1:
            out = _1D_search(self.__data,self.__shape,ele)
        elif len(self.__shape) == 2:
            out = _2D_search(self.__data,self.__shape,ele)
        elif len(self.__shape) == 3:
            out = _3D_search(self.__data,self.__shape,ele)
        else:
            raise Exception("out of range for search_element")
        return out

    def _scalar_mul(self,scalar):
        _scalar_data(self.__data,scalar,1)

    def _scalar_dev(self,scalar):
        if scalar == 0:
            raise Exception("Divison with 0!!")
        _scalar_data(self.__data, scalar, 2)

    def _scalar_sum(self, scalar):
        _scalar_data(self.__data, scalar, 3)

    def _scalar_sub(self, scalar):
        _scalar_data(self.__data, scalar, 4)

    def _compare_data(self,data):
        if self.__data == data:
            return True
        else:
            return False

    def _array_addition(self,array):
        if self.shape != array.shape:
            raise Exception("Incompatibile shapes!!")
        i = 0
        out = []
        for ele in array:
            out.append(ele+self.__data[i])
            i+=1

        ret_array = BaseArray(self.shape, dtype=self.dtype, data=out)
        return ret_array

    def _array_subtraction(self,array):
        if self.shape != array.shape:
            raise Exception("Incompatibile shapes!!")
        i = 0
        out = []
        for ele in array:
            out.append(ele-self.__data[i])
            i+=1

        ret_array = BaseArray(self.shape,dtype=self.dtype, data=out)

        return ret_array

    def _log(self,base):
        for i in range(0,len(self.__data)):
            self.__data[i]= log(self.__data[i],base)

    def _exp(self):
        for i in range(0,len(self.__data)):
            self.__data[i] = exp(self.__data[i])

    def _mul(self,array):

        if self.shape[0] == array.shape[1]:
            return _array_mul(self,array)
        else:
            raise Exception("Incompatibile shapes!!")


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

def _1D_to_string(arr):
    out = ""

    for ele in arr:
        out += str(ele)+" "

    return out

def _2D_to_string(arr):
    out = ""
    i = 0

    sec_dim = arr.shape[1]

    tmp_str = ""

    for ele in arr:
        if i == sec_dim:
            out += tmp_str + "\n"
            tmp_str = ""
            i = 0

        tmp_str += str(ele) + " "
        i += 1

    out += tmp_str + "\n"
    return out

def _3D_to_string(arr):
    out = ""
    i = 0
    j = 0

    sec_dim = arr.shape[1]
    fir_dim = arr.shape[0]

    tmp_str = ""

    for ele in arr:
        if i == sec_dim:
            out += tmp_str + "\n"
            tmp_str = ""
            i = 0
            j += 1

        if j == fir_dim:
            out += "\n"
            j = 0

        tmp_str += str(ele) + " "
        i += 1

    out += tmp_str + "\n"
    return out

def _sort_by_row(data,shape):
    out = []
    row = []
    indx = 0
    while len(out) != len(data):
        for i in range(indx,indx+shape[1]):
            row.append(data[i])

        _merge_sort(row)
        for ele in row:
            out.append(ele)

        row = []
        indx += shape[1]
    return out

def _sort_by_collumn(data,shape):
    out = []
    tmp = []
    collumn = []
    indx = 0
    iter = 1
    while len(tmp) != len(data):
        while len(collumn) != shape[0]:
            collumn.append(data[indx])
            indx+=shape[1]

        _merge_sort(collumn)
        for ele in collumn:
            tmp.append(ele)

        indx = iter
        iter+=1
        collumn = []

    indx = 0
    iter = 1
    while len(out) != len(data):
        out.append(tmp[indx])
        indx+=shape[0]
        if indx > len(tmp)-1:
            indx = iter
            iter+=1

    return out

##3#################################################################

def first_while(arr,first_half, second_half,indxArr, first_half_len,second_half_len):
    i = indxArr[0]
    j = indxArr[1]
    k = indxArr[2]

    while i < first_half_len and j < second_half_len:
        if first_half[i] < second_half[j]:
            arr[k] = first_half[i]
            i += 1
        else:
            arr[k] = second_half[j]
            j += 1
        k += 1

    indxArr[0] = i
    indxArr[1] = j
    indxArr[2] = k


def secondWhile(arr,first_half,indxArr,first_half_len):
    while indxArr[0] < first_half_len:
        arr[indxArr[2]] = first_half[indxArr[0]]
        indxArr[0] += 1
        indxArr[2] += 1

def thirdWhile(arr,second_half,indxArr,second_half_len):
    while indxArr[1] < second_half_len:
        arr[indxArr[2]] = second_half[indxArr[1]]
        indxArr[1] += 1
        indxArr[2] += 1



#pomoc : http://interactivepython.org/courselib/static/pythonds/SortSearch/TheMergeSort.html
def _merge_sort(arr):
    if len(arr) == 1:
        return

    mid = int(len(arr)/2)
    first_half = arr[:mid]
    second_half = arr[mid:]

    _merge_sort(first_half)
    _merge_sort(second_half)

    indxArr = [0,0,0]
    first_half_len = len(first_half)
    second_half_len = len(second_half)

    first_while(arr,first_half,second_half,indxArr, first_half_len,second_half_len)

    secondWhile(arr,first_half, indxArr, first_half_len)
    thirdWhile(arr, second_half, indxArr, second_half_len)

#################################################################


def _1D_search(data,shape,ele):
    out = []
    i = 0
    for i in range(0,shape[0]):
        if data[i] == ele:
            out.append(i)
    return out

def _2D_search(data,shape,ele):
    out = []
    indxs = []
    for i in range(0,len(data)):
        if data[i] == ele:
            indxs.append(i)

    x = 0
    y = 0
    width = shape[1]
    for indx in indxs:
        x = int(indx / width)
        y = indx % width
        out.append([x,y])

    return out

def _3D_search(data,shape,ele):
    out = []
    indxs = []
    for i in range(0, len(data)):
        if data[i] == ele:
            indxs.append(i)

    x = 0
    y = 0
    z = 0
    width = shape[1]
    height = shape[0]
    for indx in indxs:
        x = int(indx / width) % height
        y = indx % width
        z = int(indx / (width * height))
        out.append([x, y, z])

    return out

def _scalar_data(data,scalar,option):
    for i in range(0,len(data)):
        if option == 1:
            data[i] *= scalar
        if option == 2:
            data[i] = float( data[i]/scalar )
        if option == 3:
            data[i] += scalar
        if option == 4:
            data[i] -= scalar


def _array_mul(array1,array2):

    if len(array1.shape) == 1:
        arr1_row_size = 1
        arr1_coll_size = array1.shape[0]
    else:
        arr1_row_size = array1.shape[0]
        arr1_coll_size = array1.shape[1]

    if len(array1.shape) == 1:
        arr2_row_size = 1
        arr2_coll_size = array2.shape[0]
    else:
        arr2_row_size = array2.shape[0]
        arr2_coll_size = array2.shape[1]

    out_size = arr1_row_size*arr2_coll_size
    out = []
    row = []
    row_iter = 0
    collumn = []
    coll_iter = 0
    holder = 0

    while len(out) != out_size:
        for i in range(0,arr1_coll_size):
            row.append(array1[row_iter,i])

        while coll_iter != arr2_coll_size:

            for i in range(0,arr2_row_size):
                collumn.append(array2[i,coll_iter])

            for i in range(0,len(row)):
                holder += row[i]*collumn[i]
            out.append(holder)

            holder = 0
            collumn = []
            coll_iter+=1

        row = []
        coll_iter=0
        row_iter+=1

    return BaseArray((arr1_row_size,arr2_coll_size),dtype=int,data=(out))