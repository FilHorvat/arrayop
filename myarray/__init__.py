from myarray.basearray import BaseArray

if __name__ == '__main__':
    '''
    a = BaseArray((2, 4), dtype=int, data=(1,2,3,4,5,6,7,8))
    b = BaseArray((4, 2, 2), dtype=int, data=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16))
    print(a._to_string())
   
    a = BaseArray((2, 4), dtype=int, data=(2,5,3,4,7,6,9,1))
    print(a._to_string())
    a._sort_array()
    print(a._to_string())
    
    a = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
    b = BaseArray((4, 2, 2), dtype=int, data=(1, 2, 3, 4, 1, 6, 7, 8, 1, 10, 11, 1, 13, 1, 15, 1))
    b = BaseArray((3, 3, 2), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2, 0, 0, 0, 2, 0, 0, 0, 2, 2))
    print(b._to_string())
    print(b._search_element(0))
    
    a = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
    b = BaseArray((4, 2, 2), dtype=int, data=(1, 2, 3, 4, 1, 6, 7, 8, 1, 10, 11, 1, 13, 1, 15, 1))
    b._scalar_dev(2)
    print(b._to_string())

    compare_data = [0.5, 1.0, 1.5, 2.0, 0.5, 3.0, 3.5, 4.0, 0.5, 5.0, 5.5, 0.5, 6.5, 0.5, 7.5, 0.5]
    print(b._compare_data(compare_data))
    
    a = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
    b = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))

    a = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
    b = BaseArray((2, 4), dtype=int, data=(-1, -2, -3, -1, -5, -6, -7, -1))
    c = BaseArray((2, 4),dtype=int,)

    a = a._array_addition(b)
    print(c._to_string())
    print(a._to_string())
    if a._is_equal(c):
        print(True)
    
    a = BaseArray((2, 4), dtype=int, data=(1, 2, 3, 1, 5, 6, 7, 1))
    a._exp()
    print(a._to_string())
  
    a = BaseArray((3, 4), dtype=int, data=(4, 6, 2, -2, 0, 2, -3, 7, 1, -5, 5, 9))
    a = BaseArray((5,),dtype=int, data=(2,-3,10,5,8))
    print(a._to_string())
    a._sort_array(2)
    print(a._to_string())
    
    a = BaseArray((2, 3), dtype=int, data=(1,2,3,4,5,6))
    b = BaseArray((3, 2), dtype=int, data=(7,8,9,10,11,12))
    print(a._mul(b)._to_string())
    a = BaseArray((3, 4), dtype=int, data=(4, 6, 2, -2, 0, 2, -3, 7, 1, -5, 5, 9))
    a = BaseArray((5,), dtype=int, data=(2, -3, 10, 5, 8))
    print(len(list(a)))
    
    '''